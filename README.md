# How to build and start docker-compose

## Start

```
docker-compose up --build
```

## Stop

```
docker-compose down
```