from typing import List
from models import Product
from fastapi import FastAPI, HTTPException
from pymongo import MongoClient

app = FastAPI()

# Connexion à MongoDB
client = MongoClient("mongodb://172.17.0.1:27017/")
db = client["carpart"]
collection = db["stocks"]

# Vérification de la connexion à MongoDB
try:
    client.server_info()  # Cette instruction lance une exception si la connexion échoue
    print("Connected to MongoDB!")
except Exception as e:
    print(f"Connection to MongoDB failed: {e}")

# Route pour ajouter un ou plusieurs nouveaux produits
@app.post("/add_products")
async def add_products(products: List[Product]):
    try:
        # Créer une liste de documents à partir des données du modèle Pydantic
        products_data = [
            {
                "name": product.name,
                "description": product.description,
                "quantity": product.quantity
            }
            for product in products
        ]

        # Insérer les nouveaux produits dans la base de données
        result = collection.insert_many(products_data)

        # Récupérer les identifiants des produits nouvellement insérés
        new_product_ids = [str(product_id) for product_id in result.inserted_ids]

        return {"message": "Products added successfully", "product_ids": new_product_ids}
    except Exception as e:
        # En cas d'une exception, renvoyer une erreur HTTP 500 avec les détails de l'erreur
        raise HTTPException(status_code=500, detail=str(e))


@app.put("/update_product/{product_id}")
async def update_product(product_id, product: Product):
    # Mettre à jour un produit dans la base de données MongoDB
    collection.update_one({"_id": product_id}, {"$set": product.dict()})
    return {"message": "Product updated successfully"}


@app.delete("/delete_product/{product_id}")
async def delete_product(product_id):
    # Supprimer un produit de la base de données MongoDB
    collection.delete_one({"_id": product_id})
    return {"message": "Product deleted successfully"}


# Ajoutez les routes pour modifier, supprimer et accéder aux produits de la base de données.
@app.get("/get_product/{product_id}")
async def get_product(product_id: str):
    try:
        product_id_obj = ObjectId(product_id)

        product = collection.find_one({"_id": product_id_obj})

        if product is None:
            raise HTTPException(status_code=404, detail="Product not found")

        # Convertion de l'ObjectId en chaînes
        product["_id"] = str(product["_id"])

        return product
    
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    

# Récupération de tous les produits de la base de données
@app.get("/get_all_products")
async def get_all_products():
    try:
        products = list(collection.find())

        # Convertion des ObjectId en chaînes
        for product in products:
            product["_id"] = str(product["_id"])

        return {"products": products}
    
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


# Lancez l'application avec Uvicorn
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)