from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import os
import mysql.connector

from dotenv import load_dotenv

load_dotenv() 

app = FastAPI()

# Modèle pour représenter les données d'un client
class Client(BaseModel):
    nom: str
    prenom: str
    email: str
    nb_commandes: int


db_host = os.getenv("DB_HOST")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
db_name = os.getenv("DB_NAME")

print(db_host, db_user, db_password, db_name)

# Connexion à la base de données MySQL
mydb = mysql.connector.connect(
    host=db_host,
    user=db_user,
    password=db_password,
    database=db_name
)

# Route pour ajouter un client
@app.post("/add_client")
async def add_client(client: Client):
    cursor = mydb.cursor()
    sql = "INSERT INTO clients (nom, prenom, email, nb_commandes) VALUES (%s, %s, %s, %s)"
    val = (client.nom, client.prenom, client.email, client.nb_commandes)
    cursor.execute(sql, val)
    mydb.commit()
    return {"message": "Client ajouté avec succès"}


@app.put("/update_client/{client_id}")
async def update_client(client_id: int, client: Client):
    cursor = mydb.cursor()
    sql = "UPDATE clients SET nom = %s, prenom = %s, email = %s, nb_commandes = %s WHERE id = %s"
    val = (client.nom, client.prenom, client.email, client.nb_commandes, client_id)
    cursor.execute(sql, val)
    mydb.commit()
    return {"message": f"Fiche client {client_id} mise à jour avec succès"}


@app.delete("/delete_client/{client_id}")
async def delete_client(client_id: int):
    cursor = mydb.cursor()
    sql = "DELETE FROM clients WHERE id = %s"
    val = (client_id,)
    cursor.execute(sql, val)
    mydb.commit()
    return {"message": f"Client {client_id} supprimé avec succès"}


@app.get("/get_client/{client_id}")
async def get_client(client_id: int):
    cursor = mydb.cursor(dictionary=True)
    sql = "SELECT * FROM clients WHERE id = %s"
    val = (client_id,)
    cursor.execute(sql, val)
    client = cursor.fetchone()
    if not client:
        raise HTTPException(status_code=404, detail="Client not found")
    return client


@app.get("/get_all_clients")
async def get_all_clients():
    cursor = mydb.cursor(dictionary=True)
    sql = "SELECT * FROM clients"
    cursor.execute(sql)
    clients = cursor.fetchall()
    return {"clients": clients}
